import logging
logging.basicConfig(level=logging.DEBUG)
from spyne import Application, rpc, ServiceBase,Integer,Unicode,AnyDict,ComplexModel,Double,Iterable
from spyne.protocol.json import JsonDocument
from spyne.protocol.soap import Soap11
from spyne.protocol.dictdoc import DictDocument
from spyne.protocol.http import HttpRpc
from spyne.server.wsgi import WsgiApplication

from spyne.protocol.xml import XmlDocument
from pandas import read_csv
import numpy as np
import requests

from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score



class ToPredict(ComplexModel):
    traffic = Double

class ZoneInfo(ComplexModel):
    name = Unicode
    traffic = Integer