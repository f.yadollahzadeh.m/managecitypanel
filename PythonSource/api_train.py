import linearRegression 
import urls
from libs import (ToPredict,
Double,requests,np,Application,Soap11,
WsgiApplication,Iterable,Unicode,
ZoneInfo,ServiceBase,rpc)

URL=urls.getinfourl
  
# sending get request and saving the response as response object 
r = requests.get(url = URL)   
# extracting data in json format 
josninput = r.json() 
info=josninput['elements']
data=[]

for i in info:
    data.append(list(i.values()))
np_array = np.array(data)

# the linear regression found by sickitlearn 
# which can be plotted by matplotlib library
regr=linearRegression.FindLinearRegression(np_array)


class TrafficData(ServiceBase):
    @rpc(Unicode, _returns=Iterable(ZoneInfo))
    def trafficOfYear(self,year):  
        for i in data:
            if year==i[0] :
                user=ZoneInfo()
                user.name=str(i[1])
                user.traffic=i[7]
                yield user

    @rpc(Unicode, _returns=Iterable(ToPredict))
    def predictTraffic(self,inarr):
        try:        
            arr=inarr.split(',')
            for i in range(0,len(arr)):
                arr[i]=float(arr[i])
            l=list()
            l.append(arr)
            reshaped_array=l
            # this is the prediction made by linear regression
            prediction=regr.predict(reshaped_array)
            t=ToPredict()
            t.traffic=prediction[0,0]
            yield t
        except:
            t=ToPredict()
            l=list()
            l.append(t)
            yield l


application = Application([TrafficData],
    tns='spyne.examples.traffic',
    in_protocol=Soap11(),
    out_protocol=Soap11()
)

if __name__ == '__main__':
    # You can use any Wsgi server. Here, we chose
    # Python's built-in wsgi server but you're not
    # supposed to use it in production.
    from wsgiref.simple_server import make_server
    wsgi_app = WsgiApplication(application)
    server = make_server('0.0.0.0', 8000, wsgi_app)
    server.serve_forever()