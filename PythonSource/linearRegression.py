# Code source: Jaques Grobler
# License: BSD 3 clause
from libs import np,datasets, linear_model

def FindLinearRegression(array):
    lenoftraindata=array.shape[1]
    lenofcols=array.shape[1]
    b_new_new=[]
    for el in array[0:, lenofcols-1:lenofcols]:
        b_new_new.append(el)
    # Load the zoneInfos dataset
    zoneInfos_X= array[0:, 1:lenoftraindata-1]
    zoneInfos_y=b_new_new
    # Split the data into training/testing sets
    zoneInfos_X_train = zoneInfos_X[:-2]
    # Split the targets into training/testing sets
    zoneInfos_y_train = zoneInfos_y[:-2]
    # Create linear regression object
    regr = linear_model.LinearRegression()
    # Train the model using the training sets
    regr.fit(zoneInfos_X_train, zoneInfos_y_train,)
    return regr

