﻿using LiveCharts;
using LiveCharts.Wpf;
using MaterialDesignThemes.Wpf;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using System.Xml.Serialization;

namespace ManageCityPanel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public readonly Dictionary<string, string> Messages = new Dictionary<string, string>() { ["NotEnoughParking"] = "پارکینگ به اندازه کافی وجود ندارد" };
        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> Formatter { get; set; }
        int index = 3;//How many people usually live in 100 meters
        public readonly Dictionary<string, string> Urls = new Dictionary<string, string>() { ["AllInfo"] = "http://localhost:9090/server/admin/zoneinfos_j_zonedistance/views/zoneinfos_j_zonedistance" };
        double parkingindexmeter = 10;//area of one parking
        public MainWindow()
        {
            InitializeComponent();
            BuildChart();
        }
        public List<Element> GetAllInfo()
        {
            var client = new RestClient(Urls["AllInfo"]);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var res = JsonConvert.DeserializeObject<Root>(response.Content);
            return res.elements;
        }
        public void BuildChart()
        {
            var elements = GetAllInfo();
            Func<ChartPoint, string> labelPoint = chartPoint =>
                string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation);
            pieChart1.Series = new SeriesCollection();
            pieChart1.LegendLocation = LegendLocation.Bottom;
            pieChart2.Series = new SeriesCollection();
            pieChart2.LegendLocation = LegendLocation.Bottom;


            var name = new List<string>();
            var y = new List<double>();
            var melk = new List<double>();
            foreach (var elem in elements.OrderBy(a => a.mantaghe_name.Length).ThenBy(a => a.mantaghe_name))
            {
                var temp = new PieSeries
                {
                    Title = elem.mantaghe_name.Replace("mantaghe", "منطقه"),
                    Values = new ChartValues<double> { elem.pop },
                    PushOut = 15,
                    DataLabels = true,
                    LabelPoint = labelPoint
                };
                pieChart1.Series.Add(temp);
                temp = new PieSeries
                {
                    Title = elem.mantaghe_name.Replace("mantaghe", "منطقه"),
                    Values = new ChartValues<double> { elem.workingpop },
                    PushOut = 15,
                    DataLabels = true,
                    LabelPoint = labelPoint
                };
                pieChart2.Series.Add(temp);
                y.Add(elem.daramad_97);
                melk.Add(elem.miangin);
                name.Add(elem.mantaghe_name.Replace("mantaghe", "منطقه"));
            }

            SeriesCollection = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = "درآمد",
                    Values = new ChartValues<double>(y)
                }
            };
            SeriesCollection.Add(new ColumnSeries
            {
                Title = "قیمت ملک",
                Values = new ChartValues<double>(melk)
            });

            Labels = name.ToArray();
            Formatter = value => value.ToString("N");
            //adding series will update and animate the chart automatically

            DataContext = this;
        }

        public IEnumerable<PermitPerson> GetAllPermits(string filter = "", int zone = 0)
        {
            var client = new RestClient("http://localhost:9090/server/admin/person_j_permit/views/person_j_permit/" );
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            if(!string.IsNullOrWhiteSpace(filter))
                request.AddParameter("lastname", filter);
            if(zone!=0)
                request.AddParameter("zone", zone);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            var res = JsonConvert.DeserializeObject<GetPermits>(response.Content).elements;
            return res;
        }

        public PermitPerson GetSinglePermit(string id)
        {
            var wherequery = "?id="+id;
            var client = new RestClient("http://localhost:9090/server/admin/person_j_permit/views/person_j_permit/" + wherequery);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            var res = JsonConvert.DeserializeObject<GetPermits>(response.Content).elements;
            return res.SingleOrDefault();
        }


        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            //show charts
            GridChart.Visibility = Visibility.Visible;
        }
        private void ShowCharts_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            //show charts
            GridChart.Visibility = Visibility.Visible;
            GridApiCaller.Visibility = Visibility.Hidden;

            mainImg.Visibility = Visibility.Hidden;
        }

        private void TreeViewItem_MouseDown(object sender, MouseButtonEventArgs e)
        {
            GridChart.Visibility = Visibility.Hidden;
            mainImg.Visibility = Visibility.Hidden;
            GridApiCaller.Visibility = Visibility.Visible;
            var permit = GetAllPermits().FirstOrDefault();
            FillPermitRequest(permit);
        }
        private void FillPermitRequest(PermitPerson permit)
        {
            Floors.Text = "" + 4;
            ZoneItems.SelectedIndex = 0;
            AreaTextBox.Text = permit.zirbana.ToString();
            Parking.Text = permit.parking.ToString();
            googleLink.NavigateUri = new Uri(permit.googleearthlink);
            FirstName.Text = permit.firstname;
            LastName.Text = permit.lastname;
            NationalCode.Text = permit.nationalcode;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Sumit_Click(object sender, RoutedEventArgs e)
        {
            var z = ZoneItems.SelectedIndex + 1;
            var elements = GetAllInfo();
            var n = ("mantaghe" + z);
            var element = elements.First(z => z.mantaghe_name == n);
            var pop = element.pop;
            var income = element.daramad_97;
            var price_per_meter = element.miangin;
            var withcarpop = element.withcarpop;
            var workingpop = element.workingpop;
            var currenttraffic = element.traffic;
            var f = Floors.Text;
            var p = Convert.ToDouble(Parking.Text);
            var Area = AreaTextBox.Text;
            var people = Convert.ToDouble(Area) / 100 * index;
            //probability of people in the building having a car
            var prob = Convert.ToDouble(withcarpop) / Convert.ToDouble(pop);
            var d = (p / parkingindexmeter) - (people * prob / parkingindexmeter);

            var l = new List<string>()
            {
                z+"",""+income,""+workingpop,""+(pop + people),""+price_per_meter,""+withcarpop
            };
            var client = new RestClient("http://localhost:8000?wsdl");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/xml");
            request.AddParameter("application/xml",
                "<s11:Envelope xmlns:s11='http://schemas.xmlsoap.org/soap/envelope/'>" +
                    "<s11:Body>" +
                        "<ns1:predictTraffic xmlns:ns1='spyne.examples.traffic'>" +
                        "<!-- optional -->" +
                            "<!-- This element may be left empty if xsi:nil='true' is set. -->" +
                            "<ns1:inarr>" + string.Join(",", l) + "</ns1:inarr>" +
                        "</ns1:predictTraffic>" +
                    "</s11:Body>" +
                "</s11:Envelope>", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            string xml = response.Content;
            var serializer = new XmlSerializer(typeof(Envelope));
            Envelope result;

            using (TextReader reader = new StringReader(xml))
            {
                result = (Envelope)serializer.Deserialize(reader);
            }
            var type = "درصد افزایش";
            var estimatedTraffic = Convert.ToDouble(result.Body.PredictTrafficResponse.PredictTrafficResult.ToPredict.Traffic);
            var percentTrafficChange = ((estimatedTraffic - currenttraffic) / currenttraffic) * 100;
            if (percentTrafficChange < 0)
            {
                percentTrafficChange *= -1;
                type = "درصد کاهش";
            }
            double parkingNeeded = 0.0;
            if (d < 0)
            {
                parkingNeeded = Math.Ceiling(d * -1 / 10);
                //this means parking spots won't probably be enough
                Alerts.ShowDialog(Messages["NotEnoughParking"]);
            }
            ProccessResultWindow proccessResultWindow = new ProccessResultWindow();
            proccessResultWindow.Traffic.Content = Math.Round(percentTrafficChange,2);
            proccessResultWindow.ParkingNeede.Content = parkingNeeded;
            proccessResultWindow.typechangetraffix.Content = type;
            proccessResultWindow.ShowDialog();

            //call api and calculate traffic/or any other result

        }

        private void googleLink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start("cmd", "/c start " + googleLink.NavigateUri.ToString() + "");


        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            SearchResult.Items.Clear();
            var filter = FilterSearch.Text.Replace(System.Environment.NewLine, "");
            var zone = ZoneItems.SelectedIndex + 1;
            var allpermits = GetAllPermits(filter, zone);
            foreach (var res in allpermits)
            {
                var litem = new ListBoxItem();
                var label = new Label();
                label.Content = res.firstname + "  " + res.lastname;
                litem.Content = label;
                litem.DataContext = res.id;
                litem.MouseDoubleClick += SearchItem_Click;
                SearchResult.Items.Add(litem);
            }
        }
        
        private void SearchItem_Click(object sender, RoutedEventArgs e)
        {
            var id = ((ListBoxItem)sender).DataContext.ToString();
            var permit = GetSinglePermit(id);
            FillPermitRequest(permit);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var res = GetAllPermits().OrderByDescending(a=>
           a.dateadded).FirstOrDefault();
            var permit = GetSinglePermit(res.id);
            FillPermitRequest(permit);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var res = GetAllPermits()?.OrderBy(a =>a.dateadded).FirstOrDefault();
            var permit = GetSinglePermit(res.id);
            FillPermitRequest(permit);
        }
    }
}
