﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ManageCityPanel
{
    public class GetPermits
    {
        public string name { get; set; }
        public List<PermitPerson> elements { get; set; }
        public List<Link> links { get; set; }

    }
    public class PermitPerson
    {
        public string username { get; set; }
        public string email { get; set; }
        public string phonenumber { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string nationalcode { get; set; }
        public string id { get; set; }
        public string position { get; set; }
        public string googleearthlink { get; set; }
        public double zirbana { get; set; }
        public double parking { get; set; }
        public DateTime dateadded { get; set; }
        public string dateedited { get; set; }
        public string status { get; set; }
        public int zone { get; set; }
        public string personid { get; set; }
    }
    public class Element
    {
        public double miangin { get; set; }
        public string mantaghe_name { get; set; }
        public int pop { get; set; }
        public int withcarpop { get; set; }
        public int bisavadpop { get; set; }
        public int unisavadpop { get; set; }
        public int workingpop { get; set; }
        public double etebar_tamalok_97 { get; set; }
        public double daramad_97 { get; set; }
        public int projects_eftetah { get; set; }
        public int proejcts_start { get; set; }
        public double? mantaghe1 { get; set; }
        public double mantaghe2 { get; set; }
        public double mantaghe3 { get; set; }
        public double mantaghe4 { get; set; }
        public double? mantaghe5 { get; set; }
        public double? mantaghe6 { get; set; }
        public double? mantaghe7 { get; set; }
        public double? mantaghe8 { get; set; }
        public double? mantaghe9 { get; set; }
        public double? mantaghe10 { get; set; }
        public double? mantaghe11 { get; set; }
        public double? mantaghe12 { get; set; }
        public double? mantaghe13 { get; set; }
        public double? mantaghe14 { get; set; }
        public double mantaghe15 { get; set; }
        public double? mantaghe16 { get; set; }
        public double? mantaghe17 { get; set; }
        public double? mantaghe18 { get; set; }
        public double? mantaghe19 { get; set; }
        public double? mantaghe20 { get; set; }
        public double? mantaghe21 { get; set; }
        public double? mantaghe22 { get; set; }
        public double traffic { set; get; }

    }

    public class Link
    {
        public string rel { get; set; }
        public string href { get; set; }

    }

    public class Root
    {
        public string name { get; set; }
        public List<Element> elements { get; set; }
        public List<Link> links { get; set; }

    }
    [XmlRoot(ElementName = "ToPredict", Namespace = "libs")]
    public class ToPredict
    {
        [XmlElement(ElementName = "traffic", Namespace = "libs")]
        public string Traffic { get; set; }
    }

    [XmlRoot(ElementName = "predictTrafficResult", Namespace = "spyne.examples.traffic")]
    public class PredictTrafficResult
    {
        [XmlElement(ElementName = "ToPredict", Namespace = "libs")]
        public ToPredict ToPredict { get; set; }
    }

    [XmlRoot(ElementName = "predictTrafficResponse", Namespace = "spyne.examples.traffic")]
    public class PredictTrafficResponse
    {
        [XmlElement(ElementName = "predictTrafficResult", Namespace = "spyne.examples.traffic")]
        public PredictTrafficResult PredictTrafficResult { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Body
    {
        [XmlElement(ElementName = "predictTrafficResponse", Namespace = "spyne.examples.traffic")]
        public PredictTrafficResponse PredictTrafficResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }
        [XmlAttribute(AttributeName = "soap11env", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soap11env { get; set; }
        [XmlAttribute(AttributeName = "tns", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Tns { get; set; }
        [XmlAttribute(AttributeName = "s0", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string S0 { get; set; }
    }


    public class BuildingPermit
    {
        public Guid Id { set; get; }
        public string GoogleEarthLink { set; get; }
        public double ZirBana { set; get; }
        public double Parking { set; get; }
        public Position Position { set; get; }
        public Guid PersonId { set; get; }
    }

    public class Position
    {
        public double X { set; get; }
        public double Y { set; get; }
    }

    public class Person
    {
        public Guid Id { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string NationalCode { set; get; }
    }
}
