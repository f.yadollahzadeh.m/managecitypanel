﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MyCity.Models
{
    public class NationalCode : ValidationAttribute

    {


        public NationalCode() : base("{0} contains invalid character.")
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            string errorMessage = "";
            var _chars = (String)value;
            if (value == null)
            {
                errorMessage = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(errorMessage);
            }

            if (!Regex.IsMatch(_chars, @"^\d{10}$"))
            {
                errorMessage = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(errorMessage);
            }

            var check = Convert.ToInt32(_chars.Substring(9, 1));
            var sum = Enumerable.Range(0, 9)
                .Select(x => Convert.ToInt32(_chars.Substring(x, 1)) * (10 - x))
                .Sum() % 11;

            if (!(sum < 2 && check == sum) && !(sum >= 2 && check + sum == 11))
            {

                errorMessage = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(errorMessage);
            }

            return ValidationResult.Success;



        }
    }
    public class BuildingPermit
    {
        public string Id { set; get; }
        [Display(Name ="لینک گوگل ارث")]
        public string GoogleEarthLink { set; get; }

        [Display(Name = "وضعیت")]
        public string Status { set; get; }

        [Display(Name = "زیربنا")]
        public double ZirBana { set; get; }
        [Display(Name = "متراژ پارکینگ ساختمان")]
        public double Parking { set; get; }
        public Position Position { set; get; }
        public string PersonId { set; get; }

        [Display(Name = "تاریخ ثبت")]
        public DateTimeOffset DateAdded { set; get; }

        [Display(Name = "تاریخ اخرین ویرایش")]
        public DateTimeOffset LastEditDate { set; get; }

        [Display(Name = "منطقه")]
        public int Zone { set; get; }
    }
    public class Position
    {
        public double X { set; get; }
        public double Y { set; get; }
    }
   
}
