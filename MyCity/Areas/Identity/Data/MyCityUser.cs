﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace MyCity.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the MyCityUser class
    public class MyCityUser : IdentityUser
    {
        [Required(ErrorMessage ="وارد کردن {0} الزامی است")]
        [Display(Name="نام")]
        public string FirstName { set; get; }

        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = "وارد کردن {0} الزامی است")]
        public string LastName { set; get; }

        [Display(Name = "کد ملی")]
        [Required(ErrorMessage = "وارد کردن {0} الزامی است")]
        public string NationalCode { set; get; }
    }
}
