﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace MyCityWebsite.Data
{
    public interface IDataAccess<TC> where TC : class
    {
        IConfiguration Config { get;  }
        TC GetSthFromDatabase<TU>(TU searchIndex);
        IEnumerable<TC> GetMultipleItemsFromDatabase<U>(U searchIndex);
        IEnumerable<TC> GetTopSomeAmountOfItemsFromDatabase(int amount);
        bool AddMultipleItemToDatabase(IEnumerable<TC> obejctsToAdd);
        bool AddSingleItemToDatabase(TC objectToAdd);
        bool DeleteSthFromDatabase(TC objectToDelete);
    }


    public class SqlCommands
    {
        public static string CreatePerson = "CreatePerson";
        public static string CreatePermit = "CreatePermit";
        public static string SearchByUserName = "SearchByUserName";
    }
}