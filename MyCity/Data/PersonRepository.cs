﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using System.Data;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.Threading;
using MyCity.Models;
using MyCity.Areas.Identity.Data;

namespace MyCityWebsite.Data
{
    public class PersonRepository : IDataAccess<MyCityUser>
    {
        public SqlConnection _connection;

        public PersonRepository(IConfiguration config)
        {
            Config = config;
            _connection = new SqlConnection(ConnectionString);
        }

        public string ConnectionString
        {
            get => Config.GetSection("ConnectionStrings").Value;
            set
            {
                if (value == null) throw new ArgumentNullException(nameof(value));
            }
        }

        public IConfiguration Config { get; set; }

        #region PermitPerson


        public bool AddMultipleItemToDatabase(IEnumerable<MyCityUser> obejctsToAdd)
        {
            throw new NotImplementedException();
        }

        public bool AddSingleItemToDatabase(MyCityUser objectToAdd)
        {
            try
            {
                var person = objectToAdd;
                IEnumerable<dynamic> result = _connection.Query(SqlCommands.CreatePerson, new
                {
                    @p_id = person.Id,
                    @p_fname = person.FirstName,
                    @p_lname = person.LastName,
                    @p_nationalcode = person.NationalCode
                }, commandType: CommandType.StoredProcedure);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteSthFromDatabase(MyCityUser objectToDelete)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MyCityUser> GetMultipleItemsFromDatabase<U>(U searchIndex)
        {
            throw new NotImplementedException();
        }



        public MyCityUser GetSthFromDatabase<TU>(TU searchIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MyCityUser> GetTopSomeAmountOfItemsFromDatabase(int amount)
        {
            throw new NotImplementedException();
        }


        #endregion



    }
}
