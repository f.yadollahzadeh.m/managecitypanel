﻿using Dapper;
using Microsoft.Extensions.Configuration;
using MyCity.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MyCityWebsite.Data
{
    public class PermitRepository : IDataAccess<BuildingPermit>
    {
        public IConfiguration Config { get; }
        public SqlConnection _connection;
        public PermitRepository(IConfiguration configuration)
        {
            Config = configuration;
            _connection = new SqlConnection(configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value);
        }
        public bool AddMultipleItemToDatabase(IEnumerable<BuildingPermit> obejctsToAdd)
        {
            throw new NotImplementedException();
        }

        public bool AddSingleItemToDatabase(BuildingPermit objectToAdd)
        {
            try
            {
                var permit = objectToAdd;
                IEnumerable<dynamic> result = _connection.Query(SqlCommands.CreatePermit, new
                {
                    @p_id = permit.Id,
                    @p_pid = permit.PersonId,
                    @p_parking = permit.Parking,
                    @p_x = permit.Position.X,
                    @p_y= permit.Position.Y,
                    @p_date=permit.DateAdded.DateTime,
                    @p_zone=permit.Zone,
                    @p_zirbana= permit.ZirBana,
                    @p_googlelink= permit.GoogleEarthLink
                }, commandType: CommandType.StoredProcedure);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteSthFromDatabase(BuildingPermit objectToDelete)
        {
            throw new NotImplementedException();
        }

        public BuildingPermit GetSthFromDatabase<TU>(TU searchIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BuildingPermit> GetTopSomeAmountOfItemsFromDatabase(int amount)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BuildingPermit> GetMultipleItemsFromDatabase<U>(U searchIndex)
        {
            try
            {
                var permit = searchIndex;
                IEnumerable<BuildingPermit> result = _connection.Query<BuildingPermit>(SqlCommands.SearchByUserName, new
                {
                    @username = searchIndex
                }, commandType: CommandType.StoredProcedure);

                return result;
            }
            catch
            {
                return null;
            }
        }
    }
}
