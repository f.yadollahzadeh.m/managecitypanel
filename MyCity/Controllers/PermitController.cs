﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyCity.Areas.Identity.Data;
using MyCity.Models;
using Microsoft.SqlServer.Types;
using MyCityWebsite.Data;

namespace MyCity.Controllers
{
    public class PermitController : Controller
    {
        public IDataAccess<BuildingPermit> _permitRepo;
        private readonly UserManager<MyCityUser> _userManager;
        public PermitController(IDataAccess<BuildingPermit> permitRepo,
            UserManager<MyCityUser> userManager)
        {
            _permitRepo = permitRepo;
            _userManager = userManager;
        }
        // GET: PermitController
        public ActionResult Index()
        {
            var permitsOfuser = _permitRepo.GetMultipleItemsFromDatabase(User.Identity.Name);
            return View(permitsOfuser);
        }

        // GET: PermitController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PermitController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PermitController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BuildingPermit permit)
        {
            try
            {
                permit.Id = Guid.NewGuid().ToString();
                permit.PersonId=Guid.Parse(_userManager.GetUserId(HttpContext.User)).ToString();
                permit.DateAdded = DateTimeOffset.Now;
                var added = _permitRepo.AddSingleItemToDatabase(permit);
                if (added)
                {

                    return RedirectToAction(nameof(Index));
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: PermitController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PermitController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PermitController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PermitController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
